<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('halaman.form');
    }

    public function kirim(Request $request){
        $nama1 = $request['firstname'];
        $nama2 = $request['lastname'];
        return view('halaman.home', compact('nama1','nama2'));
    }
}
