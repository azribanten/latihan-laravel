@extends('layout.master')

@section('judul')
Halaman Pendaftaran
@endsection

@section('content')
<h1> Buat Account Baru! </h1>
<h3> Sign Up Form </h3>
<form action="/welcome" method="post"> 
    @csrf
    <label for="firstname"> Fisrt Name :</label> 
    <br><br>
    <input type="text" name=firstname>
    <br> <br>
    <label for="lastname"> Last Name :</label> 
    <br><br>
    <input type="text" name=lastname>
    <br> <br>
    <p>Gender :</p>
    <input type="radio" id="male" name="gender"><label for="male">Male</label> 
    <br>
    <input type="radio" id="female" name="gender"><label for="female">Female</label> 
    <br>
    <p>Nationality :</p>
    <select>
        <option>Indonesia</option>
        <option>Amerika</option>
        <option>Inggris</option> 
    </select>
    <br> <br>
    <p>Language Spoken:</p> 
    <input type="checkbox" id="bahasaindonesia"> <label for="bahasaindonesia">Bahasa Indonesia</label>
    <br>
    <input type="checkbox" id="english"> <label for="english">English</label>
    <br>
    <input type="checkbox" id="other"> <label for="other">Other</label>
    <br>
    <br>
    <p>Bio</p>
    <textarea></textarea>
    <br>
    
    <input  type="submit" value="kirim">

    @endsection